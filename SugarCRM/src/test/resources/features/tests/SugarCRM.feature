Feature: Logging into SugarCRM, creating a contact, deleting the contact

  @sugarLogIn
  Scenario: Logging into SugarCRM
    Given the user is on the 'SugarCRM' home page
    When the user enters the username 'admin'
    And the user enters the password 'admin'
    And the user clicks the login button
    Then they should see their username 'admin'
    And the user is on the 'Sales' , 'Contacts' page
    When the user clicks 'Create Contact'
    And the user enters the name 'Mikhail' into the first name field
    And the user enters the surname 'Smith' into the last name field
    And the user enters the number '0623022648' into the office phone field
    When the user clicks 'Save'
    Then the user should see the name 'Mikhail Smith'
    And the user is on the 'Sales' , 'Contacts' page
    When the user selects the check box on 'Mikhail Smith'
    And the user clicks 'Delete'
    Then the contact 'Mikhail Smith' will be deleted
