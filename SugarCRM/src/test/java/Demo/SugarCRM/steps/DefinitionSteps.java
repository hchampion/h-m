package Demo.SugarCRM.steps;

import net.thucydides.core.annotations.Steps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import Demo.SugarCRM.steps.serenity.EndUserSteps;
import cucumber.api.java.en.And;

public class DefinitionSteps
{

    @Steps
    EndUserSteps anna;

    @Steps
    EndUserSteps admin;

    @Given("^the user is on the '(.*)' home page$")
    public void givenTheUserIsOn(String webpage)
    {
        System.out.println("PAGE SELECT");
        switch (webpage)
        {
            case "Wikionary":
                anna.is_the_home_page();
                break;
            case "SugarCRM":
                admin.is_on_SugarCRM_Login_screen();
                break;
            default:
                break; //nothing
        }

    }

    @When("^the user looks up the definition of the word '(.*)'$")
    public void whenTheUserLooksUpTheDefinitionOf(String word)
    {
        anna.looks_for(word);
    }

    @Then("^they should see the definition '(.*)'$")
    public void thenTheyShouldSeeADefinitionContainingTheWords(String definition)
    {
        anna.should_see_definition(definition);
    }

    //SugarCRM definition steps
    @When("^the user enters the username '(.*)'$")
    public void whenTheUserEntersTheirUsername(String username) throws Throwable
    {
        admin.enters_Their_Username(username);
    }

    @When("^the user enters the password '(.*)'$")
    public void whenTheUserEntersTheirPassword(String password) throws Throwable
    {
        admin.enters_Their_Password(password);
    }

    @When("^the user clicks the login button$")
    public void whenTheUserClicksLogin() throws Throwable
    {
        admin.clicksLogin();
    }

    @Then("^they should see their username '(.*)'$")
    public void thenTheSystemValidates(String username_Being_Validated) throws Throwable
    {
        admin.validates_The_User_Logged_In(username_Being_Validated);
    }

    //navigating to add contact
    @Given("^the user is on the '(.*)' , '(.*)' page$")
    public void givenTheUserIsOnthe(String menu_heading, String Sub_menu_heading) throws Throwable
    {
        admin.isOn(menu_heading, Sub_menu_heading);
    }

    @When("^the user clicks '(.*)'$")
    public void whenTheUserClicks(String element_To_Click) throws Throwable
    {
        admin.clicks(element_To_Click);
    }

    @When("^the user enters the name '(.*)' into the first name field$")
    public void whenTheUserEntersTheName(String first_name) throws Throwable
    {
        admin.enters_The_First_Name(first_name);
    }

    @When("^the user enters the surname '(.*)' into the last name field$")
    public void whenTheUserEntersTheSurname(String last_name) throws Throwable
    {
        admin.enters_The_Last_Name(last_name);
    }

    @When("^the user enters the number '(.*)' into the office phone field$")
    public void whenTheUserEntersTheNumber(String mobile_number) throws Throwable
    {
        admin.enters_The_Mobile_Number(mobile_number);
    }

    @Then("^the user should see the name '(.*)'$")
    public void thenTheContactWillbeAdded(String expctd_val) throws Throwable
    {
        admin.validates_The_Correct_Contact_Was_Created(expctd_val);
    }

    @Given("^the user is logged in$")
    public void givenTheUserIsLoggedIn() throws Throwable
    {
        admin.is_Logged_in();
    }
    
    @When("^the user selects the check box on '(.*)'$")
    public void whenTheUserSelectsTheCheckBox(String contacts_Name) throws Throwable{
    admin.checks_the_Box_Of_Contact(contacts_Name);
    } 
    
    @Then("^the contact '(.*)' will be deleted$")
    public void thenTheContactWillBeDeleted(String contact_Name){
    admin.checks_That_Contact_Deleted(contact_Name);
    }
    

}
