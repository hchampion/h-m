package Demo.SugarCRM.steps.serenity;

import Demo.SugarCRM.pages.DictionaryPage;
import Demo.SugarCRM.pages.SugarPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import org.junit.Assert;

public class EndUserSteps
{

    DictionaryPage dictionaryPage;
    SugarPage sugarPage;

    @Step
    public void enters(String keyword)
    {
        dictionaryPage.enter_keywords(keyword);
    }

    @Step
    public void starts_search()
    {
        dictionaryPage.lookup_terms();
    }

    @Step
    public void should_see_definition(String definition)
    {
        assertThat(dictionaryPage.getDefinitions(), hasItem(containsString(definition)));
    }

    @Step
    public void looks_for(String term)
    {
        enters(term);
        starts_search();
    }

    @Step
    public void is_the_home_page()
    {
        dictionaryPage.open();
    }

    @Step
    public void is_on_SugarCRM_Login_screen()
    {
        sugarPage.open("ye");
    }

    @Step
    public void enters_Their_Username(String username_captured)
    {
        sugarPage.enter_username(username_captured);
    }

    @Step
    public void enters_Their_Password(String password)
    {
        sugarPage.enter_password(password);
    }

    @Step
    public void clicksLogin()
    {
        sugarPage.click_Login();
    }

    @Step
    public void validates_The_User_Logged_In(String username)
    {
        sugarPage.validate_Logged_in(username);
    }

    @Step
    public void isOn(String Menu, String Link)
    {
        sugarPage.navigate_to_Menu_heading(Menu);
        sugarPage.navigate_to_Sub_menu_heading(Link);
    }

    @Step
    public void clicks(String element)
    {
        sugarPage.element_To_Click(element);
    }

    @Step
    public void enters_The_First_Name(String fname)
    {
        sugarPage.enter_First_Name(fname);
    }

    @Step
    public void enters_The_Last_Name(String lname)
    {
        sugarPage.enter_Last_Name(lname);
    }

    @Step
    public void enters_The_Mobile_Number(String mobile)
    {
        sugarPage.enters_Mobile_Number(mobile);
    }

    @Step
    public void clicks_The_Save_Button()
    {
        sugarPage.clicks_The_Save_Button();
    }

    @Step
    public void validates_The_Correct_Contact_Was_Created(String expected_Value)
    {
        if (sugarPage.get_Full_Name().contains(expected_Value))
        {
            Assert.assertTrue(true);
        } else
        {
            Assert.assertTrue(false);
        }
    }

    @Step
    public void is_Logged_in()
    {
        sugarPage.open();
        sugarPage.enter_username("admin");
        sugarPage.enter_password("admin");
        sugarPage.click_Login();
    }

    @Step
    public void deletes_The_Contact(String contactsName)
    {
        sugarPage.deal_With_The_Alert();
    }

    @Step
    public void checks_the_Box_Of_Contact(String contactsName)
    {
        sugarPage.check_The_Check_Box(contactsName);
    }

    @Step
    public void clicks_Delete_Button()
    {
        sugarPage.clicks_Delete_Button();
    }

    @Step
    public void checks_That_Contact_Deleted(String fullname)
    {
        sugarPage.check_The_Check_Box(fullname);
    }

}
