package Demo.SugarCRM;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;
    
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/tests/",
        tags =
        {
            "@sugarLogIn, @sugarAddContact, @sugarDeleteContact"
        }
)
public class SugarCRM_LogIn_TestSuite
{
}
