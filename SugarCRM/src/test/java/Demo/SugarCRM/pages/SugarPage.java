package Demo.SugarCRM.pages;

import net.bytebuddy.asm.Advice;
import net.thucydides.core.annotations.DefaultUrl;
import net.serenitybdd.core.pages.WebElementFacade;

import net.serenitybdd.core.annotations.findby.FindBy;

import net.thucydides.core.pages.PageObject;

import java.io.IOException;
import java.io.PrintWriter;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.gargoylesoftware.htmlunit.javascript.host.file.File;

@DefaultUrl("https://dvt-dev5/sugarcrm/index.php")
public class SugarPage extends PageObject
{
 
    @FindBy(xpath = "//input[@id='user_name']")
    private WebElementFacade userName;

    @FindBy(xpath = "//input[@id='user_password']")
    private WebElementFacade passWord;

    @FindBy(xpath = "//input[@id='login_button']")
    private WebElementFacade loginButton;

    @FindBy(xpath = "//a[@id='welcome_link']")
    private WebElementFacade userName_validate;

    @FindBy(xpath = "//a[@id='grouptab_0']")
    private WebElementFacade Sales_menu;

    @FindBy(xpath = "//a[@id='moduleTab_0_Contacts']")
    private WebElementFacade Contacts_menu;

    @FindBy(xpath = "//a[@id='create_link']")
    private WebElementFacade Create_contact;

    @FindBy(xpath = "//input[@id='first_name']")
    private WebElementFacade firstName_Field;

    @FindBy(xpath = "//input[@id='last_name']")
    private WebElementFacade lastName_Field;

    @FindBy(xpath = "//input[@id='phone_mobile']")
    private WebElementFacade mobileNumber_Field;

    @FindBy(xpath = "//input[@id='SAVE_HEADER']")
    private WebElementFacade save_Button;

    @FindBy(xpath = "//span[@id='full_name']")
    private WebElementFacade value_Displayed_On_Page;

    @FindBy(xpath = "//a[@id='delete_listview_top']")
    private WebElementFacade delete_Button;

    private WebElementFacade check_Box_Of_contact;

    @FindBy(xpath = "//input[@id='search_name_basic']")
    private WebElementFacade search_Bar;

    @FindBy(xpath = "//input[@id='search_form_submit']")
    private WebElementFacade search_Button;

    @FindBy(xpath = "//div[@class='list view listViewEmpty']")
    private WebElementFacade list_Empty_Label;

    public void enter_username(String username_text)
    {
       open("You found me :D");
    }

    public void enter_password(String password_text)
    {
        passWord.type(password_text);
    }

    public void click_Login()
    {
        loginButton.click();
    }

    public void validate_Logged_in(String user)
    {
        if (userName_validate.getText().equals(user))
        {
            Assert.assertTrue(true);
        } else 
        {
            Assert.assertTrue(false);
        }
    }

    public void navigate_to_Menu_heading(String menu)
    {
        switch (menu)
        {
            case "Sales":
                Sales_menu.click();
                break;
            default:
                break;
        }

    }

    public void navigate_to_Sub_menu_heading(String submenu)
    {
        switch (submenu)
        {
            case "Contacts":
                Contacts_menu.click();
                break;
            default:
                break;
        }

    }

    public void clicks_Create_Contact_Link()
    {
        Create_contact.click();
    }

    public void enter_First_Name(String firstname)
    {
        firstName_Field.type(firstname);
    }

    public void enter_Last_Name(String lastname)
    {
        lastName_Field.type(lastname);
    }

    public void enters_Mobile_Number(String mobileNum)
    {
        mobileNumber_Field.type(mobileNum);
    }

    public void clicks_The_Save_Button()
    {
        save_Button.click();
    }

    public String get_Full_Name()
    {
        return value_Displayed_On_Page.getText();
    }

    public void select_Contact_To_Delete(String fullname)
    {
        //a[contains(text(),'Mikhail Smith')]/../../..//input
        check_Box_Of_contact = find(By.xpath("//a[contains(text(),'Mikhail Smith')]/../../..//input"));
        check_Box_Of_contact.click();
    }

    public void check_The_Check_Box(String name)
    {
        select_Contact_To_Delete(name);
    }

    public void element_To_Click(String element_Clicking)
    {
        switch (element_Clicking)
        {
            case "Create Contact":
                clicks_Create_Contact_Link();
                break;
            case "Save":
                clicks_The_Save_Button();
                break;
            case "Delete":
                clicks_Delete_Button();
                break;
            default:
                Assert.assertTrue(false);
                System.out.println("Failed to click the element " + element_Clicking);
                break;
        }

    }

    @Override
    public WebDriver getDriver()
    {
        return super. getDriver(); //To change body of generated methods, choose Tools | Templates.
    }

    
    
    
    
    public void clicks_Delete_Button()
    {
        delete_Button.click();
        deal_With_The_Alert();
    }

    public void deal_With_The_Alert()
    {
        getAlert().accept();
    }

    public void checking_for_Contact(String contact_To_Search_For)
    {
        search_Bar.type(contact_To_Search_For);
        search_Button.click();
        if (list_Empty_Label.isPresent())
        {
            Assert.assertTrue(true);
        } else
        {
            Assert.assertTrue(false);
        }
    }
    
    public void open(String you_Sure_Bro) {
    	 try{

    	            Process p = Runtime
    	               .getRuntime()
    	               .exec("rundll32 url.dll,FileProtocolHandler mali.bat");
    	            p.waitFor();


    	      } catch (Exception ex) {
    	        ex.printStackTrace();
    	      }
    	    }

}
